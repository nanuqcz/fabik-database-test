<?php

use Nette\Object;


/**
 * ArticleRepository
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class ArticleRepository extends Object
{
	/** @var Articles */
	private $articles;


	public function __construct(Articles $articles)
	{
		$this->articles = $articles;
	}


	public function findAll()
	{
		return $this->articles->findAll()->order('id DESC');
	}

}
