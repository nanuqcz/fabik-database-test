<?php

use Fabik\Database\ActiveRow;


/**
 * Article
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Article extends ActiveRow
{

	public function getTags()
	{
		$article2tags = $this->related('article_2_tag')
							->where('tag.deleted', 0);

		$tags = array();
		foreach ($article2tags as $article2tag) {
			$tags[] = $article2tag->tag;
		}

		return $tags;
	}

}
