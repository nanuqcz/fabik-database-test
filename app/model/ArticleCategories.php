<?php

use Fabik\Database\Table;


/**
 * ArticleCategories
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class ArticleCategories extends Table
{
	/** @var string */
	protected $name = 'article_category';
}
