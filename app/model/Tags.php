<?php

use Fabik\Database\Table;


/**
 * Tags
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Tags extends Table
{
	/** @var string */
	protected $name = 'tag';
}
