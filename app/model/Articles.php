<?php

use Fabik\Database\Table;


/**
 * Articles
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Articles extends Table
{
	/** @var string */
	protected $name = 'article';
}
