<?php

namespace App;

use ArticleRepository;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	/** @var ArticleRepository */
	private $articleRepository;


	public function injectArticles(ArticleRepository $articleRepository)
	{
		$this->articleRepository = $articleRepository;
	}


	public function renderDefault()
	{
		$this->template->articles = $this->articleRepository->findAll();
	}

}
